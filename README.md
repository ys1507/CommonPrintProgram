# 公用标签打印客户端

#### 介绍
通用标签打印客户端，可以自定义打印模板标签 支持批量打印 可以定制模板

#### 软件架构
软件架构说明
c# 
Netframework4.5
HttpListener 创建一个本地的WEB服务接口  使程序可以调用这个接口打印标签



#### 使用说明

1、先安装 bartender 软件
至于软件安装包，这里就不提供了，可以自行到网上搜索下载
打开DEBUG 下面提供的bother.btw 模版显示如下


3、启动 标签打印客户端
右键以管理员身份运行 CommonPrintProgram.exe




如果只是本地打印就不需要改IP地址
如果需要通过本地局域网调用 关闭自动打印修改成本机的IP 地址
本机IP址可以通过 CMD 命令行：输入ipconfig 查看





再开启自动打印 

日志显示 Listening.....表示成功
如出错启动异常 拒绝访问 重新右键用管理员运行项目，



调用方式
get : http://192.168.1.7:8980
参数：paramsStr 
JSON 示例字符串：
{name:"量见智屏",type:"建设工程施工图咨询审查/屏幕类",code:"CD-2019222",dutyman:"吴亦凡",qrcode:"https://inp2.baidu.com/km/asset/km_asset_card/kmAssetCard.do?method=view&fdId=53C487B9-CA52-4ceb-9CD1-87C35E65E55D",dept:"综合办"}

因为GET传输中文 有乱码的情况 所以传输入的数据 经过两次编码，先用urlencode 再用base64加密
各语言调用 请用各自语言的方法加密码之后 再传输 结果显示如下





使用POSTMAN测试 如果出现以下情况 返回503 说明端口被占用 换一个端口就可以解决问题




经过千辛万苦 终于可以了
返回信息 {"statusCode":200,"message":""}

客户端显示日志如下 说明获取到一条打印数据


本地如果有多台打印机，可以自行选择打印机打印，但最后只装一台打印机，以免出错

#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)